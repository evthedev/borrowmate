Rails.application.routes.draw do

  root 'pages#home'


  devise_for :users, 
             :path => '', 
             :path_names => {:sign_in => 'login', :sign_out => 'logout', :edit => 'profile'},
             :controllers => {:omniauth_callbacks => 'omniauth_callbacks'}

  resources :users, only: [:show]

  resources :things

  resources :photos

  resources :things do
    resources :rentals, only: [:create]
  end

  resources :conversations, only: [:index, :create] do
    resources :messages, only: [:index, :create]
  end


  get '/preload' => 'rentals#preload'
  get '/preview' => 'rentals#preview'
  get '/your_borrowings' => 'rentals#your_borrowings'
  get '/your_lendings' => 'rentals#your_lendings'

end
