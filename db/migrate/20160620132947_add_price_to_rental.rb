class AddPriceToRental < ActiveRecord::Migration
  def change
    add_column :rentals, :price, :integer
  end
end
