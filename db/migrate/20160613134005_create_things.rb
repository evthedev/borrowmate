class CreateThings < ActiveRecord::Migration
  def change
    create_table :things do |t|
      t.string :category
      t.string :name
      t.integer :price
      t.boolean :active
      t.references :user, index: true, foreign_key: true
      t.string :location
      t.string :condition
      t.text :description

      t.timestamps null: false
    end
  end
end
