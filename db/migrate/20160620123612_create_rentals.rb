class CreateRentals < ActiveRecord::Migration
  def change
    create_table :rentals do |t|
      t.references :user, index: true, foreign_key: true
      t.references :thing, index: true, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.integer :total

      t.timestamps null: false
    end
  end
end
