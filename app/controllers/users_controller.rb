class UsersController < ApplicationController

	def show
		@user = User.find(params[:id])
		@things = @user.things
	end

end