class PhotosController < ApplicationController
	def destroy
		@photo = Photo.find(params[:id])
		thing = @photo.thing

		@photo.destroy
		@photos = Photo.where(thing_id: thing.id)

		respond_to :js
	end

end
