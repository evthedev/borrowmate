class RentalsController < ApplicationController
	before_action :authenticate_user!

	def preload
		thing = Thing.find(params[:thing_id])
		today = Date.today
		rentals = thing.rentals.where("start_date >= ? OR end_date >= ?", today, today)

		render json: rentals
	end

	def preview
		start_date = Date.parse(params[:start_date])
		end_date = Date.parse(params[:end_date])

		output = {
			conflict: is_conflict(start_date, end_date)
		}

		render json: output
	end



	def create
		@rental = current_user.rentals.create(rental_params)
		redirect_to @rental.thing, notice: 'Your rental has been created'
	end

	def your_borrowings
		@borrowings = current_user.rentals
	end

	def your_lendings
		@things = current_user.things
	end

	private
		def is_conflict (start_date, end_date)
			thing = Thing.find(params[:thing_id])
			check = thing.rentals.where('? < start_date AND end_date < ?', start_date, end_date)
			check.size > 0? true: false
		end

		def rental_params
			params.require(:rental).permit(
				:start_date,
				:end_date,
				:price,
				:total,
				:thing_id
				)
		end
end
