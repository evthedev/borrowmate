class ThingsController < ApplicationController
  before_action :set_thing, only: [:show, :edit, :update]
  before_action :authenticate_user!, except: [:show]

  def index
    @things = current_user.things
  end

  def show
    @photos = @thing.photos
  end

  def new
    @thing = current_user.things.build
  end

  def create
    @thing = current_user.things.build(thing_params)

    if @thing.save
      if params[:images]
        params[:images].each do |image|
          @thing.photos.create(image: image)
        end
      end

      @photos = @thing.photos
      redirect_to edit_thing_path(@thing), notice: "Saved.."
    else
      render :new
    end
  end

  def edit
    if current_user.id == @thing.user.id
      @photos = @thing.photos
    else
      redirect_to root_path, notice: "You don't have permission to view this item"
    end
  end

  def update

    if @thing.update(thing_params)      
      if params[:images]
        params[:images].each do |image|
          @thing.photos.create(image: image)
        end
      end

      redirect_to edit_thing_path(@thing), notice: "Updated.."
    else
      render :edit
    end
  end

  private
    def set_thing
      @thing = Thing.find(params[:id])
    end

    def thing_params
      params.require(:thing).permit(
        :category,
        :name,
        :price,
        :active,
        :location,
        :condition,
        :description,
        :images
        )
    end


end
