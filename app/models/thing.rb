class Thing < ActiveRecord::Base
  belongs_to :user
  has_many :photos

  has_many :rentals
  
  geocoded_by :location
  after_validation :geocode

  validates :name, presence: true, length: {maximum: 100}
  validates :price, presence: true
  validates :location, presence: true
  validates :condition, presence: true
  validates :description, presence: true, length: {maximum: 500}
 


end
